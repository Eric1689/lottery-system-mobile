import { formatDate, getDateDiff } from './utils';

const filters = {
  formatDate,
  getDateDiff
};

export default filters;
