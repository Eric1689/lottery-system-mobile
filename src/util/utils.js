export function formatDate (timestamp) {
  var oDate = new Date();
  oDate.setTime(timestamp * 1000);

  return (
    oDate.getFullYear() +
    '-' +
    toDou(oDate.getMonth() + 1) +
    '-' +
    toDou(oDate.getDate()) +
    ' ' +
    toDou(oDate.getHours()) +
    ':' +
    toDou(oDate.getMinutes()) +
    ':' +
    toDou(oDate.getSeconds())
  );
}

function toDou (n) {
  return n < 10 ? '0' + n : n;
}

export function getDateDiff (dateTimeStamp) {
  var minute = 1000 * 60;
  var hour = minute * 60;
  var day = hour * 24;
  var month = day * 30;
  var now = new Date().getTime();
  var diffValue = now - dateTimeStamp * 1000;
  var monthC = diffValue / month;
  var weekC = diffValue / (7 * day);
  var dayC = diffValue / day;
  var hourC = diffValue / hour;
  var minC = diffValue / minute;
  if (monthC >= 1) {
    return parseInt(monthC) + '个月前';
  } else if (weekC >= 1) {
    return parseInt(weekC) + '周前';
  } else if (dayC >= 1) {
    return parseInt(dayC) + '天前';
  } else if (hourC >= 1) {
    return parseInt(hourC) + '个小时前';
  } else if (minC >= 1) {
    return parseInt(minC) + '分钟前';
  } else {
    return '刚刚';
  }
}
