import Vue from 'vue';
import fastClick from 'fastclick';
import VueLazyload from 'vue-lazyload';
import VueScroller from 'vue-scroller';
import VueClipboard from 'vue-clipboard2';
import App from './App';
import store from './store';
import router from './router';
import * as UTIL from './util/utils';
import filters from './util/filters';
import MHeader from './components/MHeader.vue';
import './assets/styles/reset.css';
import './plugins/vux';

Vue.use(VueScroller);
Vue.use(VueLazyload);
Vue.use(VueClipboard);
Vue.component('m-header', MHeader);
Vue.config.productionTip = false;

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

fastClick.attach(document.body);
Vue.prototype.$util = UTIL;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
