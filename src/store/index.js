import Vue from 'vue';
import Vuex from 'vuex';
import home from './home';
import attention from './attention';
import common from './common';
import forum from './forum';
import follow from './follow';
import match from './match';
import mine from './mine';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    common,
    home,
    attention,
    forum,
    follow,
    match,
    mine
  }
});
export default store;
