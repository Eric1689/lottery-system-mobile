// import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {};

const Getters = {};

const Actions = {
  async getHomeInfo ({ commit }, params) {
    const data = await $service.post(apiConf.homeindex, params);
    return data;
  },
  async getSign ({ commit }, params) {
    const data = await $service.post(apiConf.isSigned, params);
    return data;
  },
  async getOtherMemberInfo ({ commit }, params) {
    const data = await $service.post(apiConf.otherMemberInfo, params);
    return data;
  },
  async setSign ({ commit }, params) {
    const data = await $service.post(apiConf.toSign, params);
    return data;
  }
};

const Mutations = {};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
