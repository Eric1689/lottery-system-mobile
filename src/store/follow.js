// import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  // postList: {}
};

const Getters = {
  // postList: state => state.postList
};

const Actions = {
  async getFollowMemberList ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowMemberList, params);
    return data;
  },
  async getFollowBillList ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowBillList, params);
    return data;
  },
  async getFollowBillInfo ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowBillInfo, params);
    return data;
  },
  async getFollowBetList ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowBetList, params);
    return data;
  },
  async getFollowBill ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowBill, params);
    return data;
  }
};

const Mutations = {
  // [types.SET_POSTLIST] (state, postList) {
  //   state.postList = postList;
  // }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
