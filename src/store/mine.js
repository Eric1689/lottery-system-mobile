import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  userInfo: {}
};

const Getters = {
  userInfo: state => state.userInfo
};

const Actions = {
  async recharge ({ commit }, params) {
    const data = await $service.post(apiConf.recharge, params);
    // commit(types.SET_POSTLIST, data);
    return data;
  },
  async getRechargeList ({ commit }, params) {
    const data = await $service.post(apiConf.getRechargeList, params);
    return data;
  },
  async mineInfo ({ commit }, params = {}) {
    // let data = {};
    // const userInfos = localStorage.getItem('userInfo');
    // if (userInfos && userInfos !== 'undefined') {
    //   data = JSON.parse(userInfos);
    // } else {
    const data = await $service.post(apiConf.memberInfo, params);
    localStorage.setItem('userInfo', JSON.stringify(data));
    // }
    commit(types.SET_USER_INFO, data);
    return data;
  },
  async orderList ({ commit }, params) {
    const data = await $service.post(apiConf.orderList, params);
    return data;
  },
  async getOrderDetail ({ commit }, params) {
    const data = await $service.post(apiConf.getOrderDetail, params);
    return data;
  },
  async withdrawApply ({ commit }, params) {
    const data = await $service.post(apiConf.withdrawApply, params);
    return data;
  },
  async withdrawAbindCard ({ commit }, params) {
    const data = await $service.post(apiConf.withdrawAbindCard, params);
    return data;
  },
  async getAccountDetailInfo ({ commit }, params) {
    const data = await $service.post(apiConf.getAccountDetailInfo, params);
    return data;
  },
  async setWithdrawPsw ({ commit }, params) {
    const data = await $service.post(apiConf.setWithdrawPsw, params);
    return data;
  },
  async toWithdrawApply ({ commit }, params) {
    const data = await $service.post(apiConf.toWithdrawApply, params);
    return data;
  },
  async withdrawRecords ({ commit }, params) {
    const data = await $service.post(apiConf.withdrawList, params);
    return data;
  },
  async getCouponsList ({ commit }, params) {
    const data = await $service.post(apiConf.couponList, params);
    return data;
  },
  async getUseCouponList ({ commit }, params) {
    const data = await $service.post(apiConf.usecouponList, params);
    return data;
  },

  async getMemberScoreList ({ commit }, params) {
    const data = await $service.post(apiConf.getMemberScoreList, params);
    return data;
  },
  async setExchangeScore ({ commit }, params) {
    const data = await $service.post(apiConf.exchangeScore, params);
    return data;
  },
  async getInvitationCode ({ commit }, params) {
    const data = await $service.post(apiConf.getInvitationCode, params);
    return data;
  },
  async getUnreadMessageList ({ commit }, params) {
    const data = await $service.post(apiConf.getUnreadMessageList, params);
    return data;
  },
  async getDetailMessage ({ commit }, params) {
    const data = await $service.post(apiConf.getDetailMessage, params);
    return data;
  },
  async getPosterList ({ commit }, params) {
    const data = await $service.post(apiConf.getPosterList, params);
    return data;
  },
  async getPosterDetail ({ commit }, params) {
    const data = await $service.post(apiConf.getPosterDetail, params);
    return data;
  },
  async checkResetPswCode ({ commit }, params) {
    const data = await $service.post(apiConf.checkResetPswCode, params);
    return data;
  },
  async resetWithdrawPsw ({ commit }, params) {
    const data = await $service.post(apiConf.resetWithdrawPsw, params);
    return data;
  },
  async getVersion ({ commit }, params) {
    const data = await $service.post(apiConf.versionCheck, params);
    return data;
  }
};

const Mutations = {
  [types.SET_USER_INFO] (state, info) {
    state.userInfo = info;
  }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
