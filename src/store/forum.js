// import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  postList: {}
};

const Getters = {
  // postList: state => state.postList
};

const Actions = {
  async getPostList ({ commit }, params) {
    const data = await $service.post(apiConf.getPostList, params);
    // commit(types.SET_POSTLIST, data);
    return data;
  },
  async getPostDetail ({ commit }, params) {
    const data = await $service.post(apiConf.getPostDetail, params);
    return data;
  },
  async getArticleRemark ({ commit }, params) {
    const data = await $service.post(apiConf.getArticleRemark, params);
    return data;
  },
  async remarkArticle ({ commit }, params) {
    const data = await $service.post(apiConf.remarkArticle, params);
    return data;
  },
  async likeArticle ({ commit }, params) {
    const data = await $service.post(apiConf.likeArticle, params);
    return data;
  }

};

const Mutations = {
  // [types.SET_POSTLIST] (state, postList) {
  //   state.postList = postList;
  // }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
