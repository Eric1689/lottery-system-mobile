// import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  // postList: {}
};

const Getters = {
  // postList: state => state.postList
};

const Actions = {
  async getLiveList ({ commit }, params) {
    const data = await $service.post(apiConf.getLiveList, params);
    return data;
  },
  async getLeagueList ({ commit }, params) {
    const data = await $service.post(apiConf.getLeagueList, params);
    return data;
  },
  async getRatioLeagueList ({ commit }, params) {
    const data = await $service.post(apiConf.getRatioLeagueList, params);
    return data;
  },
  async getRatioListZQ ({ commit }, params) {
    const data = await $service.post(apiConf.getRatioListZQ, params);
    return data;
  },
  async getListZQStraight ({ commit }, params) {
    const data = await $service.post(apiConf.getListZQStraight, params);
    return data;
  },
  async goBetZq ({ commit }, params) {
    const data = await $service.post(apiConf.goBetZq, params);
    return data;
  },
  async getRatioListLQ ({ commit }, params) {
    const data = await $service.post(apiConf.getRatioListLQ, params);
    return data;
  },
  async getListLQStraight ({ commit }, params) {
    const data = await $service.post(apiConf.getListLQStraight, params);
    return data;
  },
  async goBetLq ({ commit }, params) {
    const data = await $service.post(apiConf.goBetLq, params);
    return data;
  },
  async getBonusRank ({ commit }, params) {
    const data = await $service.post(apiConf.getBonusRank, params);
    return data;
  },
  async getBonusRateRank ({ commit }, params) {
    const data = await $service.post(apiConf.getBonusRateRank, params);
    return data;
  },
  async getContinuousBonusRank ({ commit }, params) {
    const data = await $service.post(apiConf.getContinuousBonusRank, params);
    return data;
  }
};

const Mutations = {
  // [types.SET_POSTLIST] (state, postList) {
  //   state.postList = postList;
  // }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
