import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  followList: {},
  fansList: {}
};

const Getters = {
  followList: state => state.followList,
  fansList: state => state.fansList
};

const Actions = {
  async getFollowList ({ commit }, params) {
    const data = await $service.post(apiConf.getFollowList, params);
    commit(types.SET_FLLOWLIST, data);
    return data;
  },
  async getFansList ({ commit }, params) {
    const data = await $service.post(apiConf.getFansList, params);
    commit(types.SET_FANSLIST, data);
    return data;
  },
  async getFollow ({ commit }, params) {
    const data = await $service.post(apiConf.follow, params);
    return data;
  },
  async getUnFollow ({ commit }, params) {
    const data = await $service.post(apiConf.unFollow, params);
    return data;
  },
  async memberSearch ({ commit }, params) {
    const data = await $service.post(apiConf.memberSearch, params);
    return data;
  }
};

const Mutations = {
  [types.SET_FLLOWLIST] (state, followList) {
    state.followList = followList;
  },
  [types.SET_FANSLIST] (state, fansList) {
    state.fansList = fansList;
  }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
