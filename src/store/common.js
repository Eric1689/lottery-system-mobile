import * as types from './mutation-types';
import $service from '../service';
import apiConf from '../apis/api';

const State = {
  token: ''
};

const Getters = {
  token: state => state.token
};

const Actions = {
  async setLoginInfo ({ commit }, params) {
    const data = await $service.post(apiConf.login, params);
    return data;
  },
  async setLogout ({ commit }, params) {
    const data = await $service.post(apiConf.logout, params);
    return data;
  },
  async setPassword ({ commit }, params) {
    const data = await $service.post(apiConf.resetPassword, params);
    return data;
  },
  async setForgetPassword ({ commit }, params) {
    const data = await $service.post(apiConf.forgetPassword, params);
    return data;
  },
  async getAvatars ({ commit }, params) {
    const data = await $service.get(apiConf.getAvatar, params);
    return data;
  },
  async setMemberInfo ({ commit }, params) {
    const data = await $service.post(apiConf.editmemberInfo, params);
    return data;
  },
  async getRegister ({ commit }, params) {
    const data = await $service.post(apiConf.register, params);
    return data;
  },
  async sendSms ({ commit }, params) {
    const data = await $service.get(apiConf.sendSms, params);
    return data;
  },
  setAccessToken ({ commit }, token) {
    commit(types.SET_ACCESS_TOKEN, token);
  }
};

const Mutations = {
  [types.SET_LOGIN_INFO] (state, loginInfo) {
    state.loginInfo = loginInfo;
  },
  [types.SET_ACCESS_TOKEN] (state, token) {
    state.token = token;
  }
};

export default {
  namespaced: true,
  strict: true,
  state: State,
  getters: Getters,
  actions: Actions,
  mutations: Mutations
};
