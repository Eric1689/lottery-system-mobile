import Vue from 'vue';
import scroll from './vscroll';
import { Swiper, Grid, GridItem, Tabbar, TabbarItem,
  XHeader, InlineXNumber, Tab, TabItem, XTable, Marquee,
  ToastPlugin, ConfirmPlugin, Datetime, Popup, XAddress, LoadingPlugin } from 'vux';

Vue.component('swiper', Swiper);
Vue.component('grid', Grid);
Vue.component('grid-item', GridItem);
Vue.component('tabbar', Tabbar);
Vue.component('tabbar-item', TabbarItem);
Vue.component('x-header', XHeader);
Vue.component('inline-x-number', InlineXNumber);
Vue.component('tab', Tab);
Vue.component('tab-item', TabItem);
Vue.component('x-table', XTable);
Vue.component('popup', Popup);
Vue.component('datetime', Datetime);
Vue.component('marquee', Marquee)
Vue.component('x-address', XAddress);
Vue.use(scroll, {componentName: 'vue-scroll'})

Vue.use(ToastPlugin);
Vue.use(ConfirmPlugin);
Vue.use(LoadingPlugin);
