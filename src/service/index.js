import service from './request';

export default {
  async get (url, params = {}) {
    const res = await service.get(url, params);
    return res;
  },
  async post (url, params = {}) {
    const res = await service.post(url, params);
    return res;
  }
};
