export const PROD_URL = 'https://www.try255.com/v1/';
export const BASE_API = 'v1';
export const SUCCESS = 0;
export const FORBIDDEN = 100;
export const ERROR = -1;
