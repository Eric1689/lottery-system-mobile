const api = {
  login: 'member/login', // 登录
  logout: 'member/logout', // 退出
  register: 'member/register', // 注册
  memberInfo: 'member/info', // 个人信息
  editmemberInfo: 'member/edit', // 编辑个人信息
  sendSms: 'common/sendSms', // 发送短信
  getAvatar: 'common/avatar', // 获取头像列表
  otherMemberInfo: 'member/getMemberInfo', // 其他会员信息
  resetPassword: 'member/resetPassword', // 编辑的登录密码
  forgetPassword: 'member/forgetPassword', // 忘记密码找回
  versionCheck: 'version/check', // APK检查更新

  // 关注
  getFollowList: 'member/getFollowList', // 关注列表
  getFansList: 'member/getFansList', // 粉丝列表
  follow: 'member/follow', // 关注
  unFollow: 'member/unFollow', // 取消关注
  memberSearch: 'member/search', // 搜索

  // 首页
  homeindex: 'home/index', // 首页数据

  // 签到
  isSigned: 'sign/isSigned', // 签到详情
  toSign: 'sign/toSign', // 去签到

  // 论坛
  getPostList: 'article/getArticleList', // 文章列表
  getPostDetail: 'article/getDetailArticle', // 文章详情
  getArticleRemark: 'article/getArticleRemark', // 获取文章评论列表
  remarkArticle: 'article/remarkArticle', // 发布评论
  likeArticle: 'article/likeArticle', // 点赞文章

  // 跟单
  getFollowMemberList: 'recommend/getMemberList', // 大神推荐
  getFollowBillList: 'bill/getList', // 跟单列表
  getFollowBillInfo: 'bill/getInfo', // 跟单详情
  getFollowBetList: 'bill/getBetList', // 跟单投注人员列表
  getFollowBill: 'bet/bill', // 跟单投注

  // 比赛相关
  getLiveList: 'live/getList', // 比分直播列表
  getLeagueList: 'league/getList', // 获取赛事列表
  getRatioLeagueList: 'ratio/getLeagueList', // 竞彩指数 - 赛事列表
  getRatioListZQ: 'ratio/getListZQ', // 竞彩指数列表 - 足球
  goBetZq: 'bet/zq', // 竞彩足彩下注、发布跟单
  getRatioListLQ: 'ratio/getListLQ', // 竞彩指数列表 - 篮球
  goBetLq: 'bet/lq', // 竞彩篮球下注、发布跟单
  getListZQStraight: 'ratio/getListZQStraight', // 竞彩指数列表 - 足球单关
  getListLQStraight: 'ratio/getListLQStraight', // 竞彩指数列表 - 篮球单关

  getBonusRank: 'billboard/bonusRank', // 土豪神榜
  getBonusRateRank: 'billboard/bonusRateRank', // 暴击之家
  getContinuousBonusRank: 'billboard/continuousBonusRank', // 暴击之家

  // 充值
  recharge: 'finance/recharge', // 会员充值
  getRechargeList: 'finance/rechargeList', // 会员充值记录列表

  // 订单
  orderList: 'order/getList', // 订单列表
  getOrderDetail: 'order/getInfo', // 订单详情

  // 提现
  withdrawApply: 'withdraw/withdrawApply', // 提现申请列表
  withdrawAbindCard: 'member/bindCard', // 绑定银行卡
  getAccountDetailInfo: 'member/getMemberAccountDetailInfo', // 设置提现密码
  setWithdrawPsw: 'withdraw/setWithdrawPsw', // 设置提现密码
  toWithdrawApply: 'withdraw/toWithdrawApply', // 发起提现
  withdrawList: 'withdraw/withdrawList', // 提现列表记录
  checkResetPswCode: 'withdraw/checkResetPswCode', // 验证提现手机验证码
  resetWithdrawPsw: 'withdraw/resetWithdrawPsw', // 重置提现密码

  couponList: 'coupon/getList', // 优惠券
  usecouponList: 'coupon/getUseList', // 用过的优惠券

  getMemberScoreList: 'member_score/getMemberScoreList', // 积分列表
  exchangeScore: 'member_score/exchangeScore', // 兑换积分
  getInvitationCode: 'member/getInvitationCode', // 邀请好友
  // 我的
  getUnreadMessageList: 'member/getUnreadMessageList', // 获取会员未读消息列表
  getDetailMessage: 'member/getDetailMessage', // 获取一条详细的消息

  // 活动
  getPosterList: 'poster/getList', // 活动列表
  getPosterDetail: 'poster/getInfo' // 活动详情

};

export default api;
