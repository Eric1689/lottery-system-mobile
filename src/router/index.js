import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/views/Layout';
import Login from '@/views/login/Login';
import Register from '@/views/login/Register';
import ForgetPwd from '@/views/login/ForgetPwd';
import Home from '@/views/home/Index';
import Live from '@/views/live/Index';
import Attention from '@/views/attention/Index';
import AttenSearch from '@/views/attention/AttenSearch';
import Mine from '@/views/mine/Index';
import Follow from '@/views/home/follow/Index';
import FollowUser from '@/views/home/follow/FollowUser';
import FollowDetail from '@/views/home/follow/FollowDetail';
import FollowLotteryDetail from '@/views/home/follow/FollowLotteryDetail';
import FollowList from '@/views/home/follow/FollowList';
import FollowRule from '@/views/home/follow/FollowRule';
import PayConfirm from '@/views/home/follow/PayConfirm';
import SelectConfirm from '@/views/home/follow/SelectConfirm';
import PayFollow from '@/views/home/follow/PayFollow';
import Forum from '@/views/home/forum/Index';
import ForumDetail from '@/views/home/forum/ForumDetail';
import Ace from '@/views/home/ace/Index';
import Signin from '@/views/home/signin/Index';
// import Login from '@/views/login/Index';
// import Register from '@/views/register/Index';
import Rank from '@/views/rank/Index';
import Setting from '@/views/mine/setting/Index';
import About from '@/views/mine/setting/About';
import WithdrawPwd from '@/views/mine/setting/WithdrawPwd';
import ReWithdrawPwd from '@/views/mine/setting/ReWithdrawPwd';
import LoginPasswd from '@/views/mine/setting/LoginPasswd';
import Avatar from '@/views/mine/setting/Avatar';
import NickName from '@/views/mine/setting/NickName';
import Football from '@/views/home/football/Index';
import FootballPay from '@/views/home/football/Pay';
import FootballSelectConfirm from '@/views/home/football/SelectConfirm';
import Basketball from '@/views/home/basketball/Index';
import BasketballPay from '@/views/home/basketball/Pay';
import BasketballSelectConfirm from '@/views/home/basketball/SelectConfirm';
import FootballSingular from '@/views/home/footballsingular/Index';
import FootballSingularPay from '@/views/home/footballsingular/Pay';
import PayZQSConfirm from '@/views/home/footballsingular/SelectConfirm';
import BasketballSingular from '@/views/home/basketballsingular/Index';
import BasketballSingularPay from '@/views/home/basketballsingular/Pay';
import PayLQSConfirm from '@/views/home/basketballsingular/SelectConfirm';
import Recharge from '@/views/mine/recharge/Index';
import RechargeRecord from '@/views/mine/recharge/RechargeRecord';
import RechargeConfirm from '@/views/mine/recharge/RechargeConfirm';
import BankList from '@/views/mine/bank/BankList'; // 我的：提现相关
import BankAdd from '@/views/mine/bank/BankAdd';
import Withdraw from '@/views/mine/bank/Withdraw';
import WithdrawRecord from '@/views/mine/bank/WithdrawRecord';
import WithdrawStatus from '@/views/mine/bank/WithdrawStatus';
import BankPassWord from '@/views/mine/bank/Password';
import BettingRecord from '@/views/mine/betting/Index'; // 我的：投注记录
import FollowOrderDetail from '@/views/mine/betting/FollowOrderDetail'; // 我的：投注记录详情
import OrderDetail from '@/views/mine/betting/OrderDetail'; // 我的：投注记录详情
import Goods from '@/views/mine/goods/Index'; // 我的：我的物品
import Message from '@/views/mine/message/Index'; // 我的：消息中心
import MessageDetail from '@/views/mine/message/Detail'; // 我的：消息中心
import Activity from '@/views/mine/activity/Index'; // 我的：优惠活动
import ActivityDetail from '@/views/mine/activity/ActivityDetail'; // 我的：优惠活动
import Invite from '@/views/mine/invite/Index'; // 我的：邀请好友

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Layout',
      component: Layout,
      redirect: '/home',
      children: [
        { path: '/home', name: 'Home', meta: { menu: true }, component: Home },
        { path: '/live', name: 'Live', meta: { menu: true }, component: Live },
        { path: '/attention', name: 'Attention', meta: { menu: true }, component: Attention },
        { path: '/mine', name: 'Mine', meta: { menu: true }, component: Mine },
        { path: '/football', name: 'Football', component: Football },
        { path: '/footballpay', name: 'FootballPay', component: FootballPay },
        { path: '/footballbill', name: 'FootballBill', component: FootballSelectConfirm },
        { path: '/basketball', name: 'Basketball', component: Basketball },
        { path: '/basketballpay', name: 'BasketballPay', component: BasketballPay },
        { path: '/basketballbill', name: 'BasketballBill', component: BasketballSelectConfirm },
        { path: '/footballsingular', name: 'FootballSingular', component: FootballSingular },
        { path: '/payZQSConfirm', name: 'payZQSConfirm', component: PayZQSConfirm },
        { path: '/footballsingularpay', name: 'FootballSingularPay', component: FootballSingularPay },
        { path: '/basketballsingular', name: 'BasketballSingular', component: BasketballSingular },
        { path: '/basketballsingularpay', name: 'BasketballSingularPay', component: BasketballSingularPay },
        { path: '/payLQSConfirm', name: 'payLQSConfirm', component: PayLQSConfirm },
        { path: '/mine/recharge', name: 'Recharge', component: Recharge },
        { path: '/mine/rechargerecord', name: 'RechargeRecord', component: RechargeRecord },
        { path: '/mine/rechargeconfirm', name: 'RechargeConfirm', component: RechargeConfirm }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/forgetPwd',
      name: 'ForgetPwd',
      component: ForgetPwd
    },
    {
      path: '/rank',
      name: 'Rank',
      component: Rank
    },
    {
      path: '/attention/search',
      name: 'AttenSearch',
      component: AttenSearch
    },
    {
      path: '/follow',
      component: Layout,
      children: [
        { path: '', name: 'Follow', meta: { keepAlive: true }, component: Follow },
        { path: 'user/:id', name: 'FollowUser', component: FollowUser },
        { path: 'detail/:id', name: 'FollowDetail', component: FollowDetail },
        { path: 'detail/lottery/:id', name: 'FollowLotteryDetail', component: FollowLotteryDetail },
        { path: 'detail/list/:id', name: 'FollowList', component: FollowList },
        { path: 'rule', name: 'FollowRule', component: FollowRule },
        { path: 'payconfirm', name: 'PayConfirm', component: PayConfirm },
        { path: 'selectconfirm', name: 'SelectConfirm', component: SelectConfirm },
        { path: 'payFollow', name: 'PayFollow', component: PayFollow }
      ]
    },
    {
      path: '/forum',
      component: Layout,
      children: [
        { path: '', name: 'Forum', component: Forum },
        { path: 'detail/:id', name: 'ForumDetail', component: ForumDetail }
      ]
    },
    {
      path: '/ace',
      component: Layout,
      children: [
        { path: '', name: 'Ace', component: Ace }
      ]
    },
    {
      path: '/signin',
      component: Layout,
      children: [
        { path: '', name: 'Signin', component: Signin }
      ]
    },
    {
      path: '/setting',
      component: Layout,
      children: [
        { path: '', name: 'Setting', component: Setting },
        { path: 'about', name: 'About', component: About },
        { path: 'withdrawPwd', name: 'withdrawPwd', component: WithdrawPwd },
        { path: 'reWithdrawPwd', name: 'reWithdrawPwd', component: ReWithdrawPwd },
        { path: 'loginpasswd', name: 'LoginPasswd', component: LoginPasswd },
        { path: 'avatar', name: 'Avatar', component: Avatar },
        { path: 'nickName', name: 'NickName', component: NickName }
      ]
    },
    {
      path: '/bank',
      redirect: '/bank/list',
      component: Layout,
      children: [
        { path: 'list', name: 'BankList', component: BankList },
        { path: 'add', name: 'BankAdd', component: BankAdd },
        { path: 'withdraw', name: 'Withdraw', component: Withdraw },
        { path: 'record', name: 'WithdrawRecord', component: WithdrawRecord },
        { path: 'password', name: 'BankPassWord', component: BankPassWord },
        { path: 'status', name: 'WithdrawStatus', component: WithdrawStatus }
      ]
    },
    // {
    //   path: '/betting/record',
    //   name: 'BettingRecord',
    //   component: BettingRecord
    // },
    // {
    //   path: '/betting/detail/:id',
    //   name: 'FollowOrderDetail',
    //   component: FollowOrderDetail
    // },
    {
      path: '/betting',
      redirect: '/betting/record',
      component: Layout,
      children: [
        { path: 'record', name: 'BettingRecord', component: BettingRecord },
        { path: 'detail/:id', name: 'OrderDetail', component: OrderDetail },
        { path: 'details/:id', name: 'FollowOrderDetail', component: FollowOrderDetail }
      ]
    },
    {
      path: '/mine/goods',
      name: 'Goods',
      component: Goods
    },
    // {
    //   path: '/mine/goods',
    //   component: Layout,
    //   children: [
    //     { path: 'integral', name: 'Integral', component: Integral },
    //     { path: 'coupons', name: 'Coupons', component: Coupons },
    //   ]
    // },
    {
      path: '/mine/message',
      name: 'Message',
      component: Message
    },
    {
      path: '/mine/messagedetail',
      name: 'MessageDetail',
      component: MessageDetail
    },
    {
      path: '/mine/activity',
      name: 'Activity',
      component: Activity
    },
    {
      path: '/mine/activity/detail/:id',
      name: 'ActivityDetail',
      component: ActivityDetail
    },
    {
      path: '/mine/invite',
      name: 'Invite',
      component: Invite
    }
  ]
});
